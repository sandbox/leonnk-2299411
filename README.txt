Views Query Debug
=================

Views Query Debug provides the following additional query debug information,
in the Views preview SQL area.

* The original SQL query (with substitutions)
* The query run through an EXPLAIN statement
* The raw query results
</ul>

Note you have to have the "Show the SQL query" enabled in Views.

There is a ticket for this to be added to the Devel module
[#2289899]
But for now this will live in a sandbox.
